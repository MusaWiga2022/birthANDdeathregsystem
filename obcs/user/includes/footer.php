 <!-- Footer Start-->
    <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                         <p>2022 Online Birth & Death Registration System.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->