<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['obcsuid']==0)) {
  header('location:logout.php');
  } else{



  ?>
  <div class="left-sidebar-pro">
            <nav id="sidebar">
                <div class="sidebar-header">
                     <?php
$uid=$_SESSION['obcsuid'];
$sql="SELECT FirstName,LastName,MobileNumber from  tbluser where ID=:uid";
$query = $dbh -> prepare($sql);
$query->bindParam(':uid',$uid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $row)
{               ?>
                    <a href="#"><img src="img/message/avatar.jpg" alt="" />
                    </a>
                    <h3><?php  echo $row->FirstName;?>  <?php  echo $row->LastName;?></h3>
                    <p><?php  echo $row->MobileNumber;?></p><?php $cnt=$cnt+1;}} ?>
                   
                </div>
                <div class="left-custom-menu-adp-wrap">
                    <ul class="nav navbar-nav left-sidebar-menu-pro">
                        <li class="nav-item">
                            <a href="dashboard.php" role="button" aria-expanded="false"><i class="fa big-icon fa-home"></i> <span class="mini-dn">Home</span> </a>
                            
                        </li>
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-envelope"></i> <span class="mini-dn">Birth Registration</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="fill-birthregform.php" class="dropdown-item">Birth Reg Form</a>
                                <a href="view-birthregform.php" class="dropdown-item">Manage Details</a>
                              
                            </div>
                        </li>
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-files-o"></i> <span class="mini-dn">Birth Report</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="healthcond.php" class="dropdown-item">Health Conditions</a>
                                <a href="birth-dates-report.php" class="dropdown-item">Between Dates</a>
                                <a href="most-illness-report.php" class="dropdown-item">Most Illnesses</a>
             
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="certificates-list.php" role="button" aria-expanded="false"><i class="fa big-icon fa-user"></i> <span class="mini-dn">Birth Certificates</span> </a>
                            
                        </li>
                    </ul>
                </div>


                <div class="left-custom-menu-adp-wrap">
                    
                <li class="nav-item">
                            <a aria-expanded="false"><i class=""></i> <span class=""></span> </a>
          
                        </li>
                   

                    <ul class="nav navbar-nav left-sidebar-menu-pro">
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-hourglass"></i> <span class="mini-dn">Death Registration</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="fill-deathregform.php" class="dropdown-item">Death Reg Form</a>
                                <a href="view-deathregform.php" class="dropdown-item">Manage Details</a>
                              
                            </div>
                        </li>
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-files-o"></i> <span class="mini-dn">Death Reports</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="specific-cod.php" class="dropdown-item">Cause of Death</a>
                                <a href="dth-between-dates-rep.php" class="dropdown-item">Between Dates</a>
                                <a href="most-common-cod.php" class="dropdown-item">Number 1 cause of Death</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="dth-certificate-list.php" role="button" aria-expanded="false"><i class="fa big-icon fa-user"></i> <span class="mini-dn">Death Certificates</span> </a>
                            
                        </li>
                        <li class="nav-item">
                            <a aria-expanded="false"><i class=""></i> <span class=""></span> </a>
                        </li>

                        <li class="nav-item">
                            <a href="search.php" role="button" aria-expanded="false"><i class="fa fa-search"></i> <span class="mini-dn">Search</span> </a>
                        </li>
                   
                    </ul>
                </div>
            </nav>
        </div><?php }  ?>